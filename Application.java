public class Application{
    public static void main(String[] args){
        Student gabe = new Student();
        gabe.age = 18;
        gabe.passing = false;
        gabe.name = "gabe";
        Student kev = new Student();
        kev.age = 18;
        kev.passing = true;
        kev.name = "kevin";
        System.out.println(gabe.age);
        System.out.println(gabe.passing);
        System.out.println(gabe.name);
        System.out.println(kev.age);
        System.out.println(kev.passing);
        System.out.println(kev.name);   
        
        gabe.sayHiToStudent();
        kev.sayHiToStudent();
        
        Student[] section3 = new Student[3];
        section3[0] = gabe;
        section3[1] = kev;
        section3[2] = new Student();
        section3[2].name = "seb";
        section3[2].age = 17;
        section3[2].passing = true;
        System.out.println(section3[0].age);
        System.out.println(section3[2].age);
        System.out.println(section3[2].passing);
        System.out.println(section3[2].name);
        
    }   
}